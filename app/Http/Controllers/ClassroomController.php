<?php

namespace App\Http\Controllers;

use App\Models\Classroom;
use Bsmlight\ApiQuery\ClassroomApiQuery;
use Bsmlight\Exceptions\ValidationException;
use Bsmlight\HttpResponse;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Nvmcommunity\Alchemist\RestfulApi\Common\Exceptions\AlchemistRestfulApiException;
use Nvmcommunity\EloquentApi\EloquentBuilder;

class ClassroomController extends Controller
{
    /**
     * @throws AlchemistRestfulApiException
     * @throws ValidationException
     */
    public function getAllClassroomByStudentAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $studentId = $request->user()->{'id'};

        $input = $request->input();

        $builder = EloquentBuilder::for(Classroom::class, ClassroomApiQuery::class, $input);

        if (! $builder->validate($errorBag)->passes()) {
            throw new ValidationException($errorBag->getErrors());
        }

        $classrooms = $builder->getBuilder()->whereHas('Cards', function (Builder $card) use ($studentId) {
            $card->withoutGlobalScopes()->where('student_id', $studentId);
        })->get();

        $classrooms->each(function (Classroom $classroom) use ($builder) {
            $classroom->handleQueryProcess($builder->getAlchemistRestfulApi());
        });

        return $httpResponse->responseCollection($classrooms);
    }

    /**
     * @throws AlchemistRestfulApiException
     * @throws ValidationException
     */
    public function getOneClassroomByStudentAction(int $id, Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $studentId = $request->user()->{'id'};

        $input = $request->input();
        $input['filtering']['id'] = $id;

        $builder = EloquentBuilder::for(Classroom::class, ClassroomApiQuery::class, $input);

        if (! $builder->validate($errorBag)->passes()) {
            throw new ValidationException($errorBag->getErrors());
        }

        $classroom = $builder->getBuilder()->whereHas('Cards', function (Builder $card) use ($studentId) {
            $card->withoutGlobalScopes()->where('student_id', $studentId);
        })->first();

        $classroom->handleQueryProcess($builder->getAlchemistRestfulApi());

        return $httpResponse->responseData($classroom->toArray());
    }
}
