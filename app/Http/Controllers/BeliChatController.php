<?php

namespace App\Http\Controllers;

use App\Models\ChatChanel;
use App\Models\ChatMessage;
use App\Models\ChatUser;
use App\Models\User;
use Bsmlight\Exceptions\ValidationException;
use Bsmlight\HttpResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Nvmcommunity\Alchemist\RestfulApi\Response\Exceptions\BadRequestException;
use Nvmcommunity\Alchemist\RestfulApi\Response\Exceptions\NotFoundException;

class BeliChatController extends Controller
{
    public function createChannelAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'avatar' => 'string|nullable',
            'host_uuid' => 'string|nullable',
            'add_user_ids' => 'array|required',
            "origin" => 'required|string'
        ]);

        if ($notification->fails()) {
            throw new BadRequestException($notification->errors()->first());
        }

        $channel = new ChatChanel();

        $channel->fill([
            'label' => $input['label'] ?? null,
            'name' => $input['origin'] . "_" . Carbon::now()->timestamp,
            'avatar' => $input['avatar'] ?? "https://cdn-icons-png.flaticon.com/256/5962/5962463.png",
            'host_uuid' => $input['host_uuid'] ?? null,
        ]);

        $channel->save();

        foreach ($input['add_user_ids'] as $id) {
            ChatUser::query()->create([
                'user_uuid' => $input['origin'] . "_" . $id,
                'channel_id' => $channel->{'id'}
            ]);
        }

        return $httpResponse->responseData($channel->toArray());
    }

    public function getAllChannelsByUuid(string $uuid, HttpResponse $httpResponse): JsonResponse
    {
        $channels = ChatChanel::query()->whereHas('Users', function (Builder $chatUser) use ($uuid) {
            $chatUser->where('user_uuid', $uuid);
        })->orderBy('updated_at', 'DESC')->get()->map(function (ChatChanel $chanel) {
            return [
                'label' => $chanel['label'],
                'name' => $chanel['name'],
                'current_message' => $chanel->Messages()->orderBy('created_at', 'DESC')->first(['message', 'type', 'user_uuid']),
                'avatar' => $chanel['avatar'],
                'updated_at' => Carbon::parse($chanel['updated_at'])->isoFormat('DD/MM/YYYY hh:mm:ss')
            ];
        })->toArray();

        return $httpResponse->responseData($channels);
    }

    public function getAllMessagesByChannelName(string $channelName, HttpResponse $httpResponse): JsonResponse
    {
        $channel = ChatChanel::query()->where('name', $channelName)->first();

        if (!$channel) {
            return $httpResponse->responseData([]);
        }

        $messages = ChatMessage::query()->where('channel_id', $channel->{'id'})->orderBy('created_at', 'DESC')->get()->map(function (ChatMessage $chatMessage) {
            return [
                'id' => $chatMessage['id'],
                'uuid' => $chatMessage['user_uuid'],
                'message' => $chatMessage['message'],
                'type' => (int)$chatMessage['type'],
                'attachment' => $chatMessage['attachment'],
                'created_at' => Carbon::parse($chatMessage['created_at'])->isoFormat('DD/MM/YYYY hh:mm:ss')
            ];
        });

        return $httpResponse->responseData($messages->toArray());
    }

    public function createMessagesAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'channel_name' => 'required|string',
            'user_uuid' => 'required|string',
            'message' => 'required|string',
            'origin' => 'required|string',
            'type' => 'nullable|integer|in:0,1,2',
            'attachment' => 'nullable|string'
        ]);

        if ($notification->fails()) {
            throw new BadRequestException($notification->errors()->first());
        }

        $channel = ChatChanel::query()->where('name', $input['channel_name'])->first();

        if (!$channel) {
            throw new NotFoundException();
        }

        $channel->update([
            'updated_at' => Carbon::now()
        ]);

        $chatMessage = new ChatMessage();

        $chatMessage->fill([
            'channel_id' => $channel['id'],
            'user_uuid' => $input['origin'] . "_" . $input['user_uuid'],
            'message' => $input['message'],
            'type' => $input['type'] ?? 0,
            'attachment' => $input['attachment'] ?? null
        ]);

        $chatMessage->save();

        try {
            $url = 'https://beli-chat.onrender.com/new_message';
//            $url = 'http://localhost:8080/new_message';
            $client = new Client();

            $client->post($url, [
                'json' => [
                    'message' => $chatMessage['message'],
                    'channel' => $channel['name'],
                    'type' => $chatMessage['type'],
                    'attachment' => $chatMessage['attachment'],
                    'user_uuid' => $chatMessage['user_uuid'],
                    'created_at' => Carbon::parse($chatMessage['created_at'])->isoFormat('DD/MM/YYYY hh:mm:ss')
                ]
            ]);
        } catch (\Exception|GuzzleException $exception) {

        }

        return $httpResponse->responseData($chatMessage->toArray());
    }
}
