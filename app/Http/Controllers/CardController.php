<?php

namespace App\Http\Controllers;

use App\Models\Card;
use Bsmlight\ApiQuery\CardApiQuery;
use Bsmlight\Exceptions\ValidationException;
use Bsmlight\HttpResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Nvmcommunity\Alchemist\RestfulApi\Common\Exceptions\AlchemistRestfulApiException;
use Nvmcommunity\EloquentApi\EloquentBuilder;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CardController extends Controller
{
    /**
     * @throws AlchemistRestfulApiException
     * @throws ValidationException
     */
    public function getAllControllerByStudentAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $input['filtering']['student_id:eq'] = $request->user()->{'id'};

        $builder = EloquentBuilder::for(Card::class, CardApiQuery::class, $input);

        if (!$builder->validate($errorBags)->passes()) {
            throw new ValidationException($errorBags->getErrors());
        }

        $cards = $builder->getBuilder()->get()->each(function (Card $card) use ($builder) {
            $card->handleQueryProcess($builder->getAlchemistRestfulApi());
        })->toArray();
        return $httpResponse->responseCollection($cards);
    }

    /**
     * @throws AlchemistRestfulApiException
     * @throws ValidationException
     */
    public function getOneControllerByStudentAction(int $id, HttpResponse $httpResponse, Request $request): JsonResponse
    {
        $input = $request->input();

        $input['filtering']['student_id:eq'] = $request->user()->{'id'};
        $input['filtering']['id:eq'] = $id;

        $builder = EloquentBuilder::for(Card::class, CardApiQuery::class, $input);

        if (!$builder->validate($errorBags)->passes()) {
            throw new ValidationException($errorBags->getErrors());
        }
        /**
         * @var Card $card
         */
        $card = $builder->getBuilder()->first();

        if (!$card) {
            throw new NotFoundHttpException();
        }

        $card->handleQueryProcess($builder->getAlchemistRestfulApi());

        return $httpResponse->responseData($card->toArray());
    }
}
