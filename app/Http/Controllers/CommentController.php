<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\StudyLog;
use Bsmlight\ApiQuery\CommentApiQuery;
use Bsmlight\ApiQuery\StudyLogApiQuery;
use Bsmlight\Exceptions\ValidationException;
use Bsmlight\HttpResponse;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Nvmcommunity\Alchemist\RestfulApi\Common\Exceptions\AlchemistRestfulApiException;
use Nvmcommunity\EloquentApi\EloquentBuilder;

class CommentController extends Controller
{
    /**
     * @throws AlchemistRestfulApiException
     * @throws ValidationException
     */
    public function getAllCommentAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();

        $builder = EloquentBuilder::for(Comment::class, CommentApiQuery::class, $input);

        if (!$builder->validate($errorBag)->passes()) {
            throw new ValidationException($errorBag->getErrors());
        }

        $comments = $builder->getBuilder()->get();

        $comments->each(function (Comment $comment) use ($builder) {
            $comment->handleQueryProcess($builder->getAlchemistRestfulApi());
        });

        return $httpResponse->responseCollection($comments);
    }

    public function createCommentAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $studentId = $request->user()->{'id'};

        $input = $request->input();

        $notification = Validator::make($input, [
            'type' => 'integer',
            'object_type' => 'string',
            'object_id' => 'integer',
            'content' => 'string',
        ]);

        if ($notification->fails()) {
            return response()->json(['errors' => $notification->errors()], 422);
        }

        $comment = new Comment();

        $comment->fill($input);

        $comment->setAttribute('user_id', $studentId);

        $comment->save();

        return $httpResponse->responseData([
            'id' => $comment['id'],
            'user_id' => $comment['user_id'],
            'content' => $comment['content'],
            'user' => $comment->getUserAttribute(),
            'created_at' => $comment['created_at'],
            'created' => $comment->getCreatedAttribute(),
            'object_type' => $comment['object_type'],
            'object_id' => $comment['object_id'],
        ]);
    }
}
