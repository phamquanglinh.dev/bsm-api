<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\StudentProfile;
use App\Models\User;
use App\Models\UserFcm;
use Bsmlight\ApiQuery\StudentApiQuery;
use Bsmlight\HttpResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Nvmcommunity\Alchemist\RestfulApi\Common\Exceptions\AlchemistRestfulApiException;
use Nvmcommunity\Alchemist\RestfulApi\Response\Exceptions\BadRequestException;
use Nvmcommunity\Alchemist\RestfulApi\Response\Exceptions\NotFoundException;
use Nvmcommunity\EloquentApi\EloquentBuilder;
use Symfony\Component\HttpKernel\Profiler\Profile;

class AuthController extends Controller
{
    /**
     * @throws AlchemistRestfulApiException
     */
    public function loginAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {

        $input = $request->input();

        $notification = Validator::make($input, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        if ($notification->fails()) {
            throw new BadRequestException($notification->errors()->first());
        }

        /**
         * @var Student $student
         */
        $student = EloquentBuilder::for(Student::class, StudentApiQuery::class, [
            'fields' => 'id,uuid,password',
            'filtering' => [
                'uuid' => $input['username'],
            ]
        ])->getBuilder()->first();

        if (!$student) {
            throw new BadRequestException('Student not found');
        }

        if (!Hash::check($input['password'], $student->getAttribute('password'))) {
            throw new BadRequestException('Wrong password');
        }

        $token = $student->createToken('Bearer');

        return $httpResponse->responseData([
            'token' => "Bearer " . $token->plainTextToken,
        ]);
    }

    /**
     * @throws AlchemistRestfulApiException
     * @throws \Bsmlight\Exceptions\ValidationException
     */
    public function getUserInformationAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $input = $request->input();
        $input['filtering']['id'] = $request->user()->{'id'};
        $builder = EloquentBuilder::for(Student::class, StudentApiQuery::class, $input);

        if (!$builder->validate($errorBag)->passes()) {
            throw new \Bsmlight\Exceptions\ValidationException($errorBag->getErrors());
        }

        /**
         * @var Student $student
         */
        $bd = $builder->getBuilder();
        $student = $bd->first();
        $student->handleQueryProcess($builder->getAlchemistRestfulApi());

        return $httpResponse->responseData($student->toArray());
    }

    /**
     * @throws ValidationException
     */
    public function updateUserInformationAction(Request $request, HttpResponse $httpResponse): JsonResponse
    {
        $rules = [
            'password' => 'nullable|string|confirmed',
            'old_password' => 'nullable|string',
            'confirm_password' => 'nullable|string|same:password',
            'name' => 'nullable|string',
            'email' => 'nullable|string',
            'phone' => 'nullable|string',
            'address' => 'nullable|string',
            'profile' => 'array'
        ];

        $validator = Validator::make($request->all(), $rules);

        $validator->after(function ($validator) use ($request) {
            $password = $request->input('password');
            $old_password = $request->input('old_password');
            $confirm_password = $request->input('confirm_password');

            if ($password || $old_password || $confirm_password) {
                if (!$password || !$old_password || !$confirm_password) {
                    $validator->errors()->add('password', 'Thông tin bị thiếu');
                    $validator->errors()->add('old_password', 'Thông tin bị thiếu');
                    $validator->errors()->add('confirm_password', 'Thông tin bị thiếu');
                }
            }
        });

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $student = Student::query()->where('id', $request->user()->{'id'})->first();

        if (!$student) {
            throw new BadRequestException('Không tìm thấy');
        }

        $student->fill(array_filter([
            'password' => Hash::make($request->input('password')),
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'updated_at' => now(),
        ], function ($value) {
            return $value != null;
        }));

        $student->save();
        StudentProfile::query()->where('user_id', $student->id)->update($request->get('profile'));

        return $httpResponse->responseMessage('Thành công');
    }

    public function saveUserFcmTokenAction(Request $request, HttpResponse $httpResponse)
    {
        $input = $request->input();

        $notification = Validator::make($input, [
            'token' => 'required|string',
            'type' => 'integer|required|in:1,2',
        ]);

        if ($notification->fails()) {
            throw new BadRequestException($notification->errors()->first());
        }

        $userFcm = new UserFcm();

        $userFcm->fill($input);

        $userFcm->setAttribute('user_id', $request->user()->id);

        $userFcm->save();

        return $httpResponse->responseMessage('Xong');
    }
}
