<?php

namespace App\Http\Controllers;

use App\Models\CardLog;
use App\Models\StudyLog;
use Bsmlight\ApiQuery\StudyLogApiQuery;
use Bsmlight\Exceptions\ValidationException;
use Bsmlight\HttpResponse;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Nvmcommunity\Alchemist\RestfulApi\Common\Exceptions\AlchemistRestfulApiException;
use Nvmcommunity\Alchemist\RestfulApi\Response\Exceptions\NotFoundException;
use Nvmcommunity\EloquentApi\EloquentBuilder;

class StudyLogController extends Controller
{
    /**
     * @throws AlchemistRestfulApiException
     */
    public function getAllStudyLogByStudentAction(Request $request, HttpResponse $httpResponse)
    {
        $studentId = $request->user()->{'id'};

        $input = $request->input();

        $builder = EloquentBuilder::for(StudyLog::class, StudyLogApiQuery::class, $input);

        if (!$builder->validate($errorBag)->passes()) {
            throw new ValidationException($errorBag->getErrors());
        }

        $studyLogs = $builder->getBuilder()->whereHas('CardLogs', function (Builder $builder) use ($studentId) {
            $builder->where('student_id', $studentId);
        })->get();

        $studyLogs->each(function (StudyLog $studyLog) use ($builder) {
            $studyLog->handleQueryProcess($builder->getAlchemistRestfulApi());
        });

        return $httpResponse->responseCollection($studyLogs);
    }

    /**
     * @throws AlchemistRestfulApiException
     * @throws ValidationException
     */
    public function getStudyLogByStudentAction(Request $request, HttpResponse $httpResponse, int $id): JsonResponse
    {
        $studentId = $request->user()->{'id'};

        $input = $request->input();

        $input['filtering']['id'] = $id;
        $input['status']['in'] = [StudyLog::WAITING_ACCEPT, StudyLog::ACCEPTED, StudyLog::WAITING_CONFIRM];

        $builder = EloquentBuilder::for(StudyLog::class, StudyLogApiQuery::class, $input);

        if (!$builder->validate($errorBag)->passes()) {
            throw new ValidationException($errorBag->getErrors());
        }
        /**
         * @var  StudyLog $studyLog
         */
        $studyLog = $builder->getBuilder()->whereHas('CardLogs', function (Builder $builder) use ($studentId) {
            $builder->where('student_id', $studentId);
        })->first();

        if (!$studyLog) {
            throw new NotFoundException();
        }

        $studyLog->handleQueryProcess($builder->getAlchemistRestfulApi());

        return $httpResponse->responseData($studyLog->toArray());
    }

    public function getCardLogByStudentAction(Request $request, HttpResponse $httpResponse, $id): JsonResponse
    {
        $studentId = $request->user()->{'id'};
        /**
         * @var CardLog $cardLog
         */
        $cardLog = CardLog::query()->where('studylog_id', $id)->where('student_id', $studentId)->first();

        if (!$cardLog) {
            throw new NotFoundException();
        }

        return $httpResponse->responseData([
            'id' => $cardLog['id'],
            'student' => $cardLog->Student()->first(),
            'card' => $cardLog->Card()->first()->append(['van_and_attended_days', 'can_use_day']),
            'status' => $cardLog['status'],
            'status_label' => $cardLog->StatusList()[$cardLog['status']],
            'notes' => $cardLog['teacher_note'],
            'day' => (int)$cardLog['day']
        ]);
    }
}
