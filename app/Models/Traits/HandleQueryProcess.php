<?php

namespace App\Models\Traits;

use Nvmcommunity\Alchemist\RestfulApi\AlchemistRestfulApi;

trait HandleQueryProcess
{
    public function handleQueryProcess(AlchemistRestfulApi $alchemistRestfulApi)
    {
        foreach (static::defineExternalFields() as $externalField) {
            if ($alchemistRestfulApi->fieldSelector()->hasField($externalField)) {
                $this->setAttribute($externalField, $this->getAttribute($externalField));
            }
        }
    }
}
