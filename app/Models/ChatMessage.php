<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    use HasFactory;

    protected $connection = 'beli-chat';
    protected $guarded = ['id'];
    protected $table = 'messages';
}
