<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;
use Nvmcommunity\Alchemist\RestfulApi\AlchemistRestfulApi;
use Symfony\Component\HttpKernel\Profiler\Profile;

class Student extends Model
{
    use HasFactory;
    use SoftDeletes;
    use HasApiTokens;

    protected $table = 'users';
    protected $guarded = ['id'];

    /**
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();
        static::addGlobalScope('student', function (Builder $builder) {
            $builder->where('role', 2);
        });
    }

    /**
     * @return HasOne
     */
    public function profile(): HasOne
    {
        return $this->hasOne(StudentProfile::class, 'user_id', 'id');
    }

    /**
     * @param AlchemistRestfulApi $getAlchemistRestfulApi
     * @return void
     */
    public function handleQueryProcess(AlchemistRestfulApi $getAlchemistRestfulApi): void
    {
        if ($getAlchemistRestfulApi->fieldSelector()->hasField('profile')) {
            $profileFields = $getAlchemistRestfulApi->fieldSelector()->flatFields('profile');

            $this->setAttribute('profile', $this->profile()?->first($profileFields));
        }

    }
}
