<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use HasFactory;

    const CARD_TRANSACTION_TYPE = 'card';
    protected $guarded = ["id"];

    protected $table = 'transactions';

    public function Creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function Acceptor(): BelongsTo
    {
        return $this->belongsTo(User::class, 'approve', 'id');
    }
}
