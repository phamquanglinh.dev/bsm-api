<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ChatChanel extends Model
{
    use HasFactory;

    protected $connection = 'beli-chat';
    protected $guarded = ['id'];
    protected $table = "channels";
    public function Users(): HasMany
    {
        return $this->hasMany(ChatUser::class, 'channel_id', 'id');
    }

    public function Messages(): HasMany
    {
        return $this->hasMany(ChatMessage::class, 'channel_id', 'id');
    }
}
