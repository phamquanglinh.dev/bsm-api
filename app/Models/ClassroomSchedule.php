<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassroomSchedule extends Model
{
    use HasFactory;

    protected $table = 'classroom_schedules';
    protected $guarded = ['id'];

    public function getWeekStringAttribute()
    {
        return $this->week_day == 8 ? "CN" : "T{$this->week_day}";
    }
}
