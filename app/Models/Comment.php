<?php

namespace App\Models;

use App\Models\Traits\HandleQueryProcess;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use HasFactory;
    use HandleQueryProcess;
    use SoftDeletes;

    protected $guarded = ["id"];

    const OBJECT_TYPE_CARD = 'card';
    const OBJECT_TYPE_STUDYLOG = 'studylog';

    const TYPE_COMMENT = 0;

    public function getUserAttribute(): Model
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->first();
    }

    public function getCreatedAttribute(): string
    {
        return Carbon::parse($this->{'created_at'})->isoFormat("DD/MM/YYYY HH:mm:ss");
    }

    public static function defineExternalFields(): array
    {
        return [
            'user',
            'created'
        ];
    }
}
