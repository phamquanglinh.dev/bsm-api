<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CardLog extends Model
{
    use HasFactory;

    const VERIFIED = 1;
    protected $guarded = ['id'];

    protected $table = 'card_logs';

    public function StudyLog(): BelongsTo
    {
        return $this->belongsTo(StudyLog::class, 'studylog_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function Student(): BelongsTo
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function Card(): BelongsTo
    {
        return $this->belongsTo(Card::class, 'card_id', 'id');
    }

    public function StatusList(): array
    {
        return [
            0 => 'Đi học, đúng giờ',
            1 => 'Đi học, muộn',
            2 => 'Đi học, sớm',
            3 => 'Vắng, có phép',
            4 => 'Vắng, không phép',
            5 => 'Không điểm danh',
        ];
    }
}
