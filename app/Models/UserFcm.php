<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFcm extends Model
{
    protected $fillable = [
        'user_id',
        'token',
        'type'
    ];

    protected $table = 'user_fcm';

    use HasFactory;
}
