<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BeliChatController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix("beli_chat")->group(function () {
    Route::post('channels', [BeliChatController::class, 'createChannelAction']);
    Route::get('channels/{uuid}', [BeliChatController::class, 'getAllChannelsByUuid']);
    Route::get('messages/{channel_name}', [BeliChatController::class, 'getAllMessagesByChannelName']);
    Route::post('messages', [BeliChatController::class, 'createMessagesAction']);
});
