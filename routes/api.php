<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BeliChatController;
use App\Http\Controllers\CardController;
use App\Http\Controllers\ClassroomController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\StudyLogController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->prefix('cards')->group(function () {
    Route::get('/', [CardController::class, 'getAllControllerByStudentAction']);
    Route::get('/{id}', [CardController::class, 'getOneControllerByStudentAction', 'id']);
});

Route::middleware('auth:sanctum')->prefix('classrooms')->group(function () {
    Route::get('/', [ClassroomController::class, 'getAllClassroomByStudentAction']);
    Route::get('/{id}', [ClassroomController::class, 'getOneClassroomByStudentAction', 'id']);
});

Route::middleware('auth:sanctum')->prefix('studylogs')->group(function () {
    Route::get('/', [StudyLogController::class, 'getAllStudyLogByStudentAction']);
    Route::get('/{id}', [StudyLogController::class, 'getStudyLogByStudentAction', 'id']);
    Route::get('/card_log/{id}', [StudyLogController::class, 'getCardLogByStudentAction', 'id']);
});

Route::middleware('auth:sanctum')->prefix('comments')->group(function () {
    Route::get('/', [CommentController::class, 'getAllCommentAction']);
    Route::post('/', [CommentController::class, 'createCommentAction']);
});

Route::prefix('auth')->group(function () {
    Route::post('login', [AuthController::class, 'loginAction']);
    Route::middleware('auth:sanctum')->group(function () {
        Route::get('user_info', [AuthController::class, 'getUserInformationAction']);
        Route::put('user_info', [AuthController::class, 'updateUserInformationAction']);
        Route::post('user_fcm', [AuthController::class, 'saveUserFcmTokenAction']);
    });
});

