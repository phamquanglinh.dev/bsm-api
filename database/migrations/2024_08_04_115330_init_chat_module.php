<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::connection('beli-chat')->create('channels', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('label')->nullable();
            $table->string("avatar")->nullable();
            $table->string("host_uuid")->nullable();
            $table->timestamps();
        });

        Schema::connection('beli-chat')->create('user_channel', function (Blueprint $table) {
            $table->id();
            $table->string('user_uuid');
            $table->integer("channel_id");
            $table->timestamps();
        });

        Schema::connection('beli-chat')->create('messages', function (Blueprint $table) {
            $table->id();
            $table->string('user_uuid');
            $table->integer("channel_id");
            $table->longText("message");
            $table->integer('type')->default(0);
            $table->string('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::connection('beli-chat')->dropAllTables();
    }
};
