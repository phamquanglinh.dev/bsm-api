<?php

namespace Bsmlight\Exceptions;

use Nvmcommunity\Alchemist\RestfulApi\Common\Notification\CompoundErrors;
use Throwable;

class ValidationException extends \Exception
{
    public function __construct(array $errors)
    {
        parent::__construct(json_encode($errors), 400, null);
    }
}
