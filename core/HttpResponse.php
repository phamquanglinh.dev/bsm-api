<?php

namespace Bsmlight;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;

class HttpResponse
{
    public function responseMessage(string $message): JsonResponse
    {
        return response()->json([
            'message' => $message,
        ]);
    }

    public function responseCollection(Collection|array $collection): JsonResponse
    {
        if ($collection instanceof Collection) {
            return response()->json($collection->toArray());
        }

        return response()->json($collection);
    }

    public function responseData(array $data): JsonResponse
    {
        return response()->json($data);
    }
}
