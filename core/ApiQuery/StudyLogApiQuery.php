<?php

namespace Bsmlight\ApiQuery;

use Nvmcommunity\Alchemist\RestfulApi\Common\Exceptions\AlchemistRestfulApiException;
use Nvmcommunity\Alchemist\RestfulApi\Common\Integrations\AlchemistQueryable;
use Nvmcommunity\Alchemist\RestfulApi\FieldSelector\Handlers\FieldSelector;
use Nvmcommunity\Alchemist\RestfulApi\ResourceFilter\Handlers\ResourceFilter;
use Nvmcommunity\Alchemist\RestfulApi\ResourceFilter\Objects\FilteringRules;
use Nvmcommunity\Alchemist\RestfulApi\ResourcePaginations\OffsetPaginator\Handlers\ResourceOffsetPaginator;
use Nvmcommunity\Alchemist\RestfulApi\ResourceSearch\Handlers\ResourceSearch;
use Nvmcommunity\Alchemist\RestfulApi\ResourceSort\Handlers\ResourceSort;

class StudyLogApiQuery extends AlchemistQueryable
{
    public static function fieldSelector(FieldSelector $fieldSelector): void
    {
        $fieldSelector->defineFieldStructure([
            'id',
            'created_by',
            'classroom_id',
            'classroom_schedule_id',
            'title',
            'content',
            'image',
            'video',
            'audio',
            'file',
            'link',
            'status',
            'notes',
            'studylog_day',
            'created_at',
            'updated_at',
            'submit_time',
            'cancel_time',
            'accept_time',
            'classroomEntity' => [],
            'supportId' => [],
            'week_day' => [],
            'schedule_text' => [],
            'teachers' => [],
            'supporters' => [],
            'statistics' => [],
            'created_at_string' => [],
            'studylog_day_string' => [],
            'creator' => [],
            'status_label' => [],
            'status_color' => []
        ])->defineDefaultFields(['id']);
    }

    public static function resourceFilter(ResourceFilter $resourceFilter): void
    {
        $resourceFilter->defineFilteringRules([
            FilteringRules::Integer('id', ['eq']),
            FilteringRules::String('title', ['eq', 'contains']),
            FilteringRules::Integer('status', ['eq', 'contains', 'in']),
        ]);
    }

    public static function resourceOffsetPaginator(ResourceOffsetPaginator $resourceOffsetPaginator): void
    {
        $resourceOffsetPaginator->defineDefaultLimit(10)->defineMaxLimit(100);
    }

    public static function resourceSearch(ResourceSearch $resourceSearch): void
    {
        $resourceSearch->defineSearchCondition('title');
    }

    /**
     * @throws AlchemistRestfulApiException
     */
    public static function resourceSort(ResourceSort $resourceSort): void
    {
        $resourceSort->defineDefaultSort('id')->defineDefaultDirection('desc')->defineSortableFields(['id', 'title']);
    }
}
