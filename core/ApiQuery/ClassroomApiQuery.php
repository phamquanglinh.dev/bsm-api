<?php

namespace Bsmlight\ApiQuery;

use Nvmcommunity\Alchemist\RestfulApi\Common\Exceptions\AlchemistRestfulApiException;
use Nvmcommunity\Alchemist\RestfulApi\Common\Integrations\AlchemistQueryable;
use Nvmcommunity\Alchemist\RestfulApi\FieldSelector\Handlers\FieldSelector;
use Nvmcommunity\Alchemist\RestfulApi\ResourceFilter\Handlers\ResourceFilter;
use Nvmcommunity\Alchemist\RestfulApi\ResourceFilter\Objects\FilteringRules;
use Nvmcommunity\Alchemist\RestfulApi\ResourcePaginations\OffsetPaginator\Handlers\ResourceOffsetPaginator;
use Nvmcommunity\Alchemist\RestfulApi\ResourceSearch\Handlers\ResourceSearch;
use Nvmcommunity\Alchemist\RestfulApi\ResourceSort\Handlers\ResourceSort;

class ClassroomApiQuery extends AlchemistQueryable
{
    public static function fieldSelector(FieldSelector $fieldSelector): void
    {
        $fieldSelector->defineFieldStructure([
            'id',
            'uuid',
            'name',
            'book',
            'avatar',
            'staff_id',
            'branch',
            'status',
            'broadcast_chat' => [],
            'broadcast_teacher_chat' => [],
            'broadcast_student_chat' => [],
            'classroom_schedule' => [],
            'schedule_last_update' => [],
            'total_meetings' => [],
            'days' => [],
            'student_attended' => [],
            'student_left' => [],
            'avg_attendance' => [],
            'total_earned' => [],
            'external_salary' => [],
            'internal_salary' => [],
            'supporter_salary' => [],
            'gross' => [],
            'gross_percent' => [],
            'gross_status' => [],
            'teachers' => [],
            'supporters' => [],
            'staff' => [],
        ])->defineDefaultFields(['id']);
    }

    public static function resourceFilter(ResourceFilter $resourceFilter): void
    {
        $resourceFilter->defineFilteringRules([
            FilteringRules::Integer('id', ['eq']),
            FilteringRules::String('name', ['eq', 'contains']),
            FilteringRules::String('uuid', ['eq', 'contains']),
        ]);
    }

    public static function resourceOffsetPaginator(ResourceOffsetPaginator $resourceOffsetPaginator): void
    {
        $resourceOffsetPaginator->defineDefaultLimit(10)->defineMaxLimit(100);
    }

    public static function resourceSearch(ResourceSearch $resourceSearch): void
    {
        $resourceSearch->defineSearchCondition('name');
    }

    /**
     * @throws AlchemistRestfulApiException
     */
    public static function resourceSort(ResourceSort $resourceSort): void
    {
        $resourceSort->defineDefaultSort('id')->defineDefaultDirection('desc')->defineSortableFields(['id', 'name']);
    }
}
