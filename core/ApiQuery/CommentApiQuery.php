<?php

namespace Bsmlight\ApiQuery;

use Nvmcommunity\Alchemist\RestfulApi\Common\Exceptions\AlchemistRestfulApiException;
use Nvmcommunity\Alchemist\RestfulApi\Common\Integrations\AlchemistQueryable;
use Nvmcommunity\Alchemist\RestfulApi\FieldSelector\Handlers\FieldSelector;
use Nvmcommunity\Alchemist\RestfulApi\ResourceFilter\Handlers\ResourceFilter;
use Nvmcommunity\Alchemist\RestfulApi\ResourceFilter\Objects\FilteringRules;
use Nvmcommunity\Alchemist\RestfulApi\ResourcePaginations\OffsetPaginator\Handlers\ResourceOffsetPaginator;
use Nvmcommunity\Alchemist\RestfulApi\ResourceSearch\Handlers\ResourceSearch;
use Nvmcommunity\Alchemist\RestfulApi\ResourceSort\Handlers\ResourceSort;

class CommentApiQuery extends AlchemistQueryable
{
    public static function fieldSelector(FieldSelector $fieldSelector): void
    {
        $fieldSelector->defineFieldStructure([
            'id',
            'user_id',
            'type',
            'object_type',
            'object_id',
            'content',
            'created_at',
            'user' => [],
            'created' => [],
        ])->defineDefaultFields(['id']);
    }

    public static function resourceFilter(ResourceFilter $resourceFilter): void
    {
        $resourceFilter->defineFilteringRules([
            FilteringRules::Integer('user_id', ['eq']),
            FilteringRules::Integer('type', ['eq']),
            FilteringRules::Integer('object_id', ['eq']),
            FilteringRules::String('object_type', ['eq']),
            FilteringRules::Integer('created_at', ['eq']),
        ]);
    }

    public static function resourceOffsetPaginator(ResourceOffsetPaginator $resourceOffsetPaginator): void
    {
        $resourceOffsetPaginator->defineDefaultLimit(10)->defineMaxLimit(100);
    }

    public static function resourceSearch(ResourceSearch $resourceSearch): void
    {
        $resourceSearch->defineSearchCondition('content');
    }

    /**
     * @throws AlchemistRestfulApiException
     */
    public static function resourceSort(ResourceSort $resourceSort): void
    {
        $resourceSort->defineDefaultSort('id')->defineDefaultDirection('desc')->defineSortableFields(['id', 'created_at']);
    }

}
