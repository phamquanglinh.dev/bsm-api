<?php

namespace Bsmlight\ApiQuery;

use Nvmcommunity\Alchemist\RestfulApi\Common\Exceptions\AlchemistRestfulApiException;
use Nvmcommunity\Alchemist\RestfulApi\Common\Integrations\AlchemistQueryable;
use Nvmcommunity\Alchemist\RestfulApi\FieldSelector\Handlers\FieldSelector;
use Nvmcommunity\Alchemist\RestfulApi\ResourceFilter\Handlers\ResourceFilter;
use Nvmcommunity\Alchemist\RestfulApi\ResourceFilter\Objects\FilteringRules;
use Nvmcommunity\Alchemist\RestfulApi\ResourcePaginations\OffsetPaginator\Handlers\ResourceOffsetPaginator;
use Nvmcommunity\Alchemist\RestfulApi\ResourceSearch\Handlers\ResourceSearch;
use Nvmcommunity\Alchemist\RestfulApi\ResourceSort\Handlers\ResourceSort;

class CardApiQuery extends AlchemistQueryable
{
    public static function fieldSelector(FieldSelector $fieldSelector): void
    {
        $fieldSelector->defineFieldStructure([
            'id',
            'uuid',
            'van',
            'van_date',
            'student_id',
            'classroom_id',
            'classroom_type',
            'commitment',
            'drive_link',
            'fee_reason',
            'bonus_reason',
            'card_status',
            'payment_plan',
            'allow_deg',
            'limit_deg',
            'original_days',
            'bonus_days',
            'original_fee',
            'promotion_fee',
            'paid_fee' => [],
            'attended_days' => [],
            'total_days' => [],
            'total_fee' => [],
            'daily_fee' => [],
            'can_use_day_by_paid' => [],
            'unpaid_fee' => [],
            'can_use_day' => [],
            'can_use_fee' => [],
            'feedback_score' => [],
            'latest_feedback_date' => [],
            'sale_status' => [],
            'sale_updated_at' => [],
            'van_and_attended_days' => [],
            "student_entity" => [],
            "classroom_entity" => [],
            "renew_type" => [],
            "last_be_attendance" => [],
            "be_attendance_warning" => [],
            "transaction_logs" => []
        ])->defineDefaultFields(['id']);
    }

    public static function resourceFilter(ResourceFilter $resourceFilter): void
    {
        $resourceFilter->defineFilteringRules([
            FilteringRules::Integer('id', ['eq']),
            FilteringRules::String('uuid', ['eq', 'contains']),
            FilteringRules::Integer('student_id', ['eq']),
        ]);
    }

    public static function resourceOffsetPaginator(ResourceOffsetPaginator $resourceOffsetPaginator): void
    {
        $resourceOffsetPaginator->defineDefaultLimit(10)->defineMaxLimit(100);
    }

    public static function resourceSearch(ResourceSearch $resourceSearch): void
    {
        $resourceSearch->defineSearchCondition('uuid');
    }

    /**
     * @throws AlchemistRestfulApiException
     */
    public static function resourceSort(ResourceSort $resourceSort): void
    {
        $resourceSort->defineDefaultSort('id')->defineDefaultDirection('desc')->defineSortableFields(['id', 'name']);
    }
}
